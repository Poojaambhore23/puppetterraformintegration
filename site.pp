# site.pp
node 'example-instance' {
  include 'webserver'
}

# modules/webserver/manifests/init.pp
class webserver {
  package { 'apache2':
    ensure => 'installed',
  }

  service { 'apache2':
    ensure  => 'running',
    enable  => true,
  }
}
